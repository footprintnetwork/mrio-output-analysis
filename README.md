# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for scripts or functions used to analyze MRIO outputs, including scripts used to create any outputs. 
* It is NOT for MRIO calculation scripts, which should go in the gfn-mrio repository


### Contents ###

* make_region_subregion_diagram.R
	* This makes "chord diagrams" for UN regions and subregions designated in GTAP regions
	* This excludes pretty much all of the "rest of ..." GTAP regions in aggregations
	

### Who do I talk to? ###

* Originally created by Evan N